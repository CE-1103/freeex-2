<<<<<<< HEAD
package cr.ac.tec.ce1103.structures.nodes;

public class Node {
	
	//Fields
	
	protected Object dato;
	protected Node next;
	
	
	// Constructors
	
	public Node(){
		setDato(null);
		setNext(null);
	}
	
	public Node(Object dato){
		this.setDato(dato);
		setNext(null);
	}
	
	public Node(Object dato, Node next){
		this.setDato(dato);
		this.setNext(next);
	}
	
	
	// getters and setters
	
	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	public Object getDato() {
		return dato;
	}

	public void setDato(Object dato) {
		this.dato = dato;
	}
}
=======
package cr.ac.tec.ce1103.structures.nodes;

public class Node {
	
	//Fields
	
	private Object dato;
	private Node next;
	
	
	// Constructors
	
	public Node(){
		setDato(null);
		setNext(null);
	}
	
	public Node(Object dato){
		this.setDato(dato);
		setNext(null);
	}
	
	public Node(Object dato, Node next){
		this.setDato(dato);
		this.setNext(next);
	}
	
	
	// getters and setters
	
	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	public Object getDato() {
		return dato;
	}

	public void setDato(Object dato) {
		this.dato = dato;
	}
}
>>>>>>> a219f2a7c36b3402420336363faf28c5c10c8946

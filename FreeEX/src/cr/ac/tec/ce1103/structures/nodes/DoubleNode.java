package cr.ac.tec.ce1103.structures.nodes;

public class DoubleNode extends Node{
	
	private DoubleNode prev;
	
	public DoubleNode(){
		super();
		prev = null;
	}
	
	public DoubleNode(Object pDato, DoubleNode prev, DoubleNode next){
		super(pDato, next);
		this.prev = prev;
	}

	/**
	 * @return the prev
	 */
	public DoubleNode getPrev() {
		return prev;
	}

	/**
	 * @param prev the prev to set
	 */
	public void setPrev(DoubleNode prev) {
		this.prev = prev;
	}
	
	
}

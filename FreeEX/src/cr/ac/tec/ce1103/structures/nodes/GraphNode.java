package cr.ac.tec.ce1103.structures.nodes;

public class GraphNode {
	
	// Fields 
	
	public final boolean isBaseStation;
	private int cost;
	private String ref;
	public final String publicKey;
	public final String name;
	
	//Constructors
	
	public GraphNode(String ref, String publicKey, int cost, boolean isBaseStation, String name){
		this.isBaseStation = isBaseStation;
		this.setCost(cost);
		this.setRef(ref);
		this.publicKey = publicKey;
		this.name = name;
	}
	
	//Getters and setters

	/**
	 * @return the cost
	 */
	public int getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public GraphNode setCost(int cost) {
		this.cost = cost;
		return this;
	}

	/**
	 * @return the ref
	 */
	public String getRef() {
		return ref;
	}

	/**
	 * @param ref the ref to set
	 */
	public void setRef(String ref) {
		this.ref = ref;
	}
	
	
	
	
}
package cr.ac.tec.ce1103.structures;

import cr.ac.tec.ce1103.structures.nodes.Node;

public class Stack <generic>{
	
	//Fields
	
	private Node head;
	
	public Stack(){
		head = null;
	}
	
	public Stack(Node head){
		this.head = head;
	}
	
	@SuppressWarnings("unchecked")
	public generic pop(){
		if (head != null){
			Object tmp = head.getDato();
			head = head.getNext();
			return ((generic) (tmp));
		} 
		return null; 
	}
	
	public Stack<generic> push(Object pDato){
		
		head = new Node(pDato, head);
		return this;
	}

}

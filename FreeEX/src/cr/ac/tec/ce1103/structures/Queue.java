package cr.ac.tec.ce1103.structures;

import java.io.IOException;

import cr.ac.tec.ce1103.functionalities.FreeEXProperties;

/**
 * 
 * @author Keylor Rojas
 *
 */
public class Queue {
	
	//Fields
	private int head;
	private int tail;
	private int size;
	private int length;
	private boolean flag;
	private Object[] queue;
	
	//Constructors
	/**
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	public Queue() throws NumberFormatException, IOException{
		setSize(Integer.parseInt(FreeEXProperties.getProperties("DEFAULT_QUEUE_SIZE")));
		head = 0;
		tail = 0;
		setLength(0);
		flag = false;
		queue = new Object[getSize()];
	}
	
	/**
	 * 
	 * @param size
	 */
	public Queue(int size){
		this.setSize(size);
		head = 0;
		tail = 0;
		setLength(0);
		flag = false;
		queue = new Object[size];
	}
	
	/**
	 * 
	 */
	private void resize(){
		this.setSize(getSize()*2);
		Object[] newQueue = new Object[this.getSize()];
		int pos = 0;
		
		for (int i = head; i < getSize(); i ++){
			newQueue[pos] = queue[i];
			pos ++;
		}
		
		if (flag){
			for (int i = 0; i - 1 < tail; i ++){
				newQueue[pos] = queue[i];
			}
		}
		
		head = 0;
		tail = pos + 1;
		flag = false;
		queue = newQueue;
	}
	
	/**
	 * 
	 * @param pDato
	 */
	public void enqueue(Object pDato){
		if (head == tail && flag){
			resize();
		}
		
		queue[tail] = pDato;
		tail ++;
		if (tail == getSize()){
			tail = 0;
		}
		
		if (tail<head){
			flag = true;
		}
		setLength(getLength() + 1);
	}
	
	/**
	 * 
	 * @return
	 */
	public Object dequeue(){
		
		Object tmp = queue[head];
		if (getLength() > 0){
			head ++;
			if (head == getSize()){
				head = 0;
				flag = false;
			}
			setLength(getLength() - 1);
		}
		return tmp;
	}
	
	// getters and setters.

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param length the length to set
	 */
	private void setLength(int length) {
		this.length = length;
	}

	/**
	 * @param size the size to set
	 */
	private void setSize(int size) {
		this.size = size;
	}

}

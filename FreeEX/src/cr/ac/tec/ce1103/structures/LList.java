package cr.ac.tec.ce1103.structures;

import cr.ac.tec.ce1103.structures.nodes.DoubleNode;

public class LList<generic extends Comparable<generic>> {
	
	private DoubleNode head;
	
	public LList(){
		setHead(null);
	}
	
	public void insert(Object pDato){
		if(getHead() != null){
			
			DoubleNode tmp = new DoubleNode(pDato, null, getHead());
			getHead().setPrev(tmp);
			setHead(tmp);
			
		} else {
			setHead(new DoubleNode(pDato, null, null));
		}		
	}
	
	public DoubleNode find(generic pDato){
		
		boolean flag = true;
		DoubleNode tmp;
		for(tmp = getHead(); tmp != null; tmp = (DoubleNode) (tmp.getNext())){
			if(pDato.compareTo(((generic)(tmp.getDato()))) != 0){
				flag = false;
			}
		}
		return tmp;
	}

	/**
	 * @return the head
	 */
	public DoubleNode getHead() {
		return head;
	}

	/**
	 * @param head the head to set
	 */
	public void setHead(DoubleNode head) {
		this.head = head;
	}

}

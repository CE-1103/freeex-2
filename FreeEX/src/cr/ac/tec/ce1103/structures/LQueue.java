package cr.ac.tec.ce1103.structures;

import cr.ac.tec.ce1103.structures.nodes.Node;

public class LQueue {
	
	Node head;
	Node tail;
	
	public LQueue(){
		head = null;
		tail = null;
	}
	
	/**
	 * 
	 * @param pDato
	 */
	public void enqueue(Object pDato){
		if (head != null){
			if(head != tail){
				tail.setNext(new Node(pDato, null));
			} else {
				tail = new Node(pDato, null);
				head.setNext(tail);
			}
		} else {
			
			tail = new Node(pDato, null);
			head = tail;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Object dequeue(){
		Object tmp = head.getDato();
		head = head.getNext();
		return tmp;
	}
}

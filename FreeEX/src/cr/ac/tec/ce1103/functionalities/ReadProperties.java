/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cr.ac.tec.ce1103.functionalities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Keylor Rojas 
 * @version 28/10/2015
 */
public class ReadProperties {
    
    InputStream input;
    Properties properties;
    
    public ReadProperties(String file) throws IOException{
        
        input = getClass().getClassLoader().getResourceAsStream(file);
        if(input != null){
            properties.load(input);
        }
        input.close();
    }
    
    public String getProperties(String key){
        return properties.getProperty(key);
    }
    
}
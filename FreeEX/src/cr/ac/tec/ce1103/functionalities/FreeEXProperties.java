package cr.ac.tec.ce1103.functionalities;

import java.io.IOException;

public class FreeEXProperties {

	private static ReadProperties PROPIEDADES = null;
	
	private FreeEXProperties(){}
	
	public static String getProperties(String key) throws IOException{
		if(PROPIEDADES == null){
			PROPIEDADES = new ReadProperties("FreeEX.properties");
		}			
		return PROPIEDADES.getProperties(key);
	}

}

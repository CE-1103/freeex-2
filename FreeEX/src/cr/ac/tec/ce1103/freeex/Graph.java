package cr.ac.tec.ce1103.freeex;

import java.io.IOException;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import cr.ac.tec.ce1103.functionalities.FreeEXProperties;
import cr.ac.tec.ce1103.structures.Stack;
import cr.ac.tec.ce1103.structures.nodes.GraphNode;

/**
 * 
 * @author krojas96
 *
 */
public class Graph{
	
	//Fields
	
	public final boolean isBaseStation;
	public final String name;
	private GraphNode[][] adjacencyMatrix;
	private Date version;
	private final int position;
	private Stack<Integer> avaibleSpace;
	private int size = Integer.parseInt(FreeEXProperties.getProperties("MAX_NODES"));
	private JSONObject namesPosition;
	public final String pKey;
	
	//Constructors
	
	/**
	 * 
	 * @param name
	 * @param pKey
	 * @param ibs
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	public Graph(String name, String pKey, boolean ibs) throws NumberFormatException, IOException, JSONException{
		int size = Integer.parseInt(FreeEXProperties.getProperties("MAX_NODES"));
		setAdjacencyMatrix(new GraphNode[size][size]);
		position = 0;
		setAvaibleSpace(new Stack<Integer>());
		for (int i = 0; i < size; i ++){
			getAvaibleSpace().push(i);
		}
		version = new Date();
		setNamesPosition(new JSONObject());
		this.name = name;
		namesPosition.put(name, position);
		this.pKey = pKey;
		isBaseStation = ibs;
	}
	
	/**
	 * 
	 * @param grafo
	 * @param name
	 * @param pKey
	 * @param ibs
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	public Graph(Graph grafo, String name, String pKey, boolean ibs) throws NumberFormatException, IOException, JSONException{
		
		this.name = name;
		size = grafo.getSize();
		adjacencyMatrix = grafo.getAdjacencyMatrix();
		avaibleSpace = grafo.getAvaibleSpace();
		Object tmpPos = avaibleSpace.pop();
		
		if(tmpPos != null){
			position = avaibleSpace.pop();
		}else {
			adjacencyMatrix = resizeMatrix(adjacencyMatrix, size);
			size = size*2;
			for (int i = size/2; i<size; i++){
				avaibleSpace.push(i);
			}
			position = avaibleSpace.pop();
		}
		namesPosition = grafo.getNamesPosition();
		namesPosition.put(name, position);
		this.pKey  = pKey;
		isBaseStation = ibs;
	}
	
	// Operations
	
	/**
	 * 
	 * @param matrix
	 * @param size
	 * @return
	 */
	private static GraphNode[][] resizeMatrix(GraphNode[][] matrix, int size){
		GraphNode[][] tmpMatrix = new GraphNode[size*2][size*2];
		for (int i = 0; i < size; i++){
			for (int j = 0; j <= size; j++){
				tmpMatrix[i][j] = matrix[i][j];
			}
		}
		return tmpMatrix;
	}
	
	/**
	 * 
	 * @param to
	 * @param cost
	 * @param pKey
	 * @param name
	 * @param isBaseStation
	 */
	public void addConnection(int to, int cost, String pKey, String name, boolean isBaseStation){
		adjacencyMatrix[position][to] = new GraphNode(null, pKey, cost, isBaseStation, name);
		adjacencyMatrix[to][position] = new GraphNode(null, this.pKey, cost, this.isBaseStation, this.name);
	}
	
	/**
	 * 
	 * @param toDelete
	 */
	public void deleteConnection(int toDelete){
		adjacencyMatrix[position][toDelete+1] = null;
		adjacencyMatrix[toDelete][position+1] = null;
	}
	
	/**
	 * 
	 */
	public void deleteConnections(){
		deleteConnections(position);
	}
	
	/**
	 * 
	 * @param position
	 */
	public void deleteConnections(int position){
		GraphNode tmp = new GraphNode(null, null, Integer.MAX_VALUE, false, null);
		for (int i = 0; i < size; i++){
			adjacencyMatrix[i][position] = tmp;
			adjacencyMatrix[position][i] = tmp;
		}
		avaibleSpace.push(position);
	}
	
	/**
	 * 
	 */
	public void floyd(){
		int path;
		for(int k = 0; k < size; k++){
			for(int i = 0; i < size; i++){
				for(int j = 0; j < size; j++){
					path = adjacencyMatrix[i][k].getCost() + adjacencyMatrix[k][j].getCost();
			        if(adjacencyMatrix[i][j].getCost() > path){
			        	adjacencyMatrix[i][j].setCost(path).setRef(adjacencyMatrix[i][0].name);
			        }
				}
			}
		}
	}
	
	/**
	 * 
	 * @param destination
	 * @return
	 * @throws JSONException
	 */
	public Stack<Integer> getPath(int destination) throws JSONException{
		return getPath(new Stack<Integer>(), destination);
	}
		
	/**
	 * 
	 * @param path
	 * @param destination
	 * @return
	 * @throws JSONException
	 */
	public Stack<Integer> getPath(Stack<Integer> path, int destination) throws JSONException{
		GraphNode tmpNode = adjacencyMatrix[destination][position+1];
		if(tmpNode.getRef() != null){
			return getPath(path.push(namesPosition.getInt(tmpNode.getRef())), destination);
		}
		return path;
	}
	
	// getters and setters
	
	/**
	 * @return the adjacencyMatrix
	 */
	public GraphNode[][] getAdjacencyMatrix() {
		return adjacencyMatrix;
	}

	/**
	 * @param adjacencyMatrix the adjacencyMatrix to set
	 */
	public void setAdjacencyMatrix(GraphNode[][] adjacencyMatrix) {
		this.adjacencyMatrix = adjacencyMatrix;
	}

	/**
	 * @return the version
	 */
	public Date getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(Date version) {
		this.version = version;
	}

	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * @return the avaibleSpace
	 */
	public Stack<Integer> getAvaibleSpace() {
		return avaibleSpace;
	}

	/**
	 * @param avaibleSpace the avaibleSpace to set
	 */
	public void setAvaibleSpace(Stack<Integer> avaibleSpace) {
		this.avaibleSpace = avaibleSpace;
	}

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * 
	 * @return
	 */
	public JSONObject getNamesPosition() {
		return namesPosition;
	}

	/**
	 * 
	 * @param namesPosition
	 */
	public void setNamesPosition(JSONObject namesPosition) {
		this.namesPosition = namesPosition;
	}

	/**
	 * 
	 * @return
	 */
	public Graph getGraph(){
		return this;
	}

	
}

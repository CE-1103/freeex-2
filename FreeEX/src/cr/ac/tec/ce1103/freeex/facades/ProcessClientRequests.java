package cr.ac.tec.ce1103.freeex.facades;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import cr.ac.tec.ce1103.functionalities.FreeEXProperties;

public class ProcessClientRequests implements Runnable{
	
	// Fields
	
	private static ProcessClientRequests INSTANCE = null;

	// Constructor
	private ProcessClientRequests(){}
	
	public static ProcessClientRequests getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ProcessClientRequests();
		}
		return INSTANCE;
	}
	
	@Override
	public void run(){
		
		while(true){
			JSONObject request = (JSONObject) (Parser.getInstance().clientRequests.dequeue());
			if(request != null){
				try {
					String action =  request.getString(FreeEXProperties.getProperties("msgType"));
					if(action == FreeEXProperties.getProperties("logReq")){
						BluetoothManager.getInstance().search(request.getString(FreeEXProperties.getProperties("name")));
					} else if (action == FreeEXProperties.getProperties("discon")){
						Parser.getInstance().getGrafo().deleteConnections();
					} else if (action == FreeEXProperties.getProperties("msgStr")){
						
					} else if (action == FreeEXProperties.getProperties("msgMedia")){
						
					} else if (action == FreeEXProperties.getProperties("msgChkIn")){
						
					}
				} catch (JSONException | IOException e) {
					e.printStackTrace();
				}
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}

<<<<<<< HEAD:FreeEX/src/cr/ac/tec/ce1103/freeEX/facades/Parser.java
package cr.ac.tec.ce1103.freeex.facades;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import cr.ac.tec.ce1103.bluetooth.BluetoothManager;
import cr.ac.tec.ce1103.freeex.Graph;
import cr.ac.tec.ce1103.functionalities.FreeEXProperties;
import cr.ac.tec.ce1103.structures.LQueue;
import cr.ac.tec.ce1103.structures.Queue;

public class Parser{
	
	LQueue bluetoothRequests;
	LQueue clientRequests;
	private static Parser INSTANCE = null;
	private Graph grafo;
	private Queue inbound;
	
	/**
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	private Parser() throws NumberFormatException, IOException, JSONException{
		clientRequests = new LQueue();
		bluetoothRequests = new LQueue();
		
		setGrafo(new Graph(null, null, false));
		
		Thread bluetoothRequests = (new Thread(ProcessBluetoothRequests.getInstance()));
		Thread clientRequests = new Thread(ProcessClientRequests.getInstance());
		
		bluetoothRequests.start();
		clientRequests.start(); 
		inbound = new Queue();
		
	}
	
	/**
	 * 
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	public static Parser getInstance() throws NumberFormatException, IOException, JSONException{
		if(INSTANCE == null){
			INSTANCE = new Parser();
		}
		return INSTANCE;
	}

	// getters and setters

	public synchronized String addBluetoothRequest(JSONObject action) throws IOException {
		bluetoothRequests.enqueue(action);
		return FreeEXProperties.getProperties("RECEIVED");
	}
	
	/**
	 * 
	 * @param action
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public String addClientRequest(JSONObject action) throws IOException, JSONException {
		if(!(action.getString(FreeEXProperties.getProperties("msg")).equals(FreeEXProperties.getProperties("logReq")))){
			clientRequests.enqueue(action);
			return FreeEXProperties.getProperties("RECEIVED");
		}
		return BluetoothManager.getInstance().checkUsr(action.getString("usr"));
	}

	/**
	 * @return the inbound
	 */
	public Object getMessages() {
		return inbound.dequeue();
	}

	/**
	 * @return the grafo
	 */
	public Graph getGrafo() {
		return grafo;
	}

	/**
	 * @param grafo the grafo to set
	 */
	public void setGrafo(Graph grafo) {
		this.grafo = grafo;
	}	
}
=======
package cr.ac.tec.ce1103.freeex.facades;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import cr.ac.tec.ce1103.freeex.Graph;
import cr.ac.tec.ce1103.functionalities.FreeEXProperties;
import cr.ac.tec.ce1103.structures.LQueue;
import cr.ac.tec.ce1103.structures.Queue;

public class Parser{
	
	LQueue bluetoothRequests;
	LQueue clientRequests;
	private static Parser INSTANCE = null;
	private Graph grafo;
	private Queue inbound;
	
	/**
	 * 
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	private Parser() throws NumberFormatException, IOException, JSONException{
		clientRequests = new LQueue();
		bluetoothRequests = new LQueue();
		
		setGrafo(new Graph(null, null, false));
		
		Thread bluetoothRequests = (new Thread(ProcessBluetoothRequests.getInstance()));
		Thread clientRequests = new Thread(ProcessClientRequests.getInstance());
		
		bluetoothRequests.start();
		clientRequests.start(); 
		inbound = new Queue();
		
	}
	
	/**
	 * 
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 * @throws JSONException
	 */
	public static Parser getInstance() throws NumberFormatException, IOException, JSONException{
		if(INSTANCE == null){
			INSTANCE = new Parser();
		}
		return INSTANCE;
	}

	// getters and setters

	public synchronized String addBluetoothRequest(JSONObject action) throws IOException {
		bluetoothRequests.enqueue(action);
		return FreeEXProperties.getProperties("RECEIVED");
	}
	
	/**
	 * 
	 * @param action
	 * @throws IOException 
	 */
	public String  addClientRequest(JSONObject action) throws IOException {
		clientRequests.enqueue(action);
		return FreeEXProperties.getProperties("RECEIVED");
	}

	/**
	 * @return the inbound
	 */
	public Object getMessages() {
		return inbound.dequeue();
	}

	/**
	 * @return the grafo
	 */
	public Graph getGrafo() {
		return grafo;
	}

	/**
	 * @param grafo the grafo to set
	 */
	public void setGrafo(Graph grafo) {
		this.grafo = grafo;
	}	
}
>>>>>>> a219f2a7c36b3402420336363faf28c5c10c8946:FreeEX/src/cr/ac/tec/ce1103/freeex/facades/Parser.java

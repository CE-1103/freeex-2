package cr.ac.tec.ce1103.freeex.facades;

public class ProcessBluetoothRequests implements Runnable{
	
	private static ProcessBluetoothRequests INSTANCE = null;

	private ProcessBluetoothRequests(){}
	
	public static ProcessBluetoothRequests getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ProcessBluetoothRequests();
		}
		return INSTANCE;
	}
	

	@Override
	public void run() {
		
	}

}

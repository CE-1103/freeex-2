package cr.ac.tec.ce1103.freeex;

import java.io.IOException;

import cr.ac.tec.ce1103.functionalities.FreeEXProperties;

public class Inbound extends MessageQueue{

	private Inbound() throws NumberFormatException, IOException {
		super(Integer.parseInt(FreeEXProperties.getProperties("DEFAULT_INBOUND_SIZE")));
	}
	
	public Inbound getInstance() throws NumberFormatException, IOException{
		if(INSTANCE == null){
			INSTANCE = new Inbound();
		}
		return (Inbound) INSTANCE;
	}

}

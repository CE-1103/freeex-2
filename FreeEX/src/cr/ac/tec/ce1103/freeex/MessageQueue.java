package cr.ac.tec.ce1103.freeex;

import java.io.IOException;

import cr.ac.tec.ce1103.functionalities.FreeEXProperties;
import cr.ac.tec.ce1103.structures.Queue;

public class MessageQueue extends Queue{
	
	protected static Object INSTANCE = null;
	protected Queue messages;
	
	protected MessageQueue() throws NumberFormatException, IOException{
		messages = new Queue(Integer.parseInt(FreeEXProperties.getProperties("DEFAULT_MESSAGE_QUEUE_SIZE")));
	}
	
	protected MessageQueue(int size) throws IOException{
		messages = new Queue(size);
	}
	
	public MessageQueue getInstance() throws NumberFormatException, IOException{
		if (INSTANCE == null){
			INSTANCE = new MessageQueue();
		}
		return (MessageQueue) INSTANCE;
	}
	
	public boolean addMessage(String[] mensaje){
		if (messages.getSize() == messages.getLength()){
			return false;
		}
		messages.enqueue(mensaje);
		return true;
	}
	
	public String[][] getMessage(){
		if(messages.getLength() != 0){
			return (String[][]) (messages.dequeue());
		}
		return null;
	}
}

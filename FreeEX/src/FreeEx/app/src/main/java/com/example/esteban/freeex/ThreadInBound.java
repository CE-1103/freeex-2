package com.example.esteban.freeex;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Esteban on 21/11/2015.
 */
public class ThreadInBound extends Thread{

    ListaConversaciones actividad;

    public ThreadInBound(ListaConversaciones actividad){
        this.actividad = actividad;
    }

    public void run(){
        while (true){
            JSONObject inbound = Parser.getInstance().getInbound();
            if (inbound!=null){
                try {
                    if (inbound.getString(FreeEXProperties.getProperties("msgType")).equals(FreeEXProperties.getProperties("clUpdate"))) {
                        String[] usuarios = (String[]) (inbound.get(FreeEXProperties.getProperties("users")));
                        actividad.setListaConectados(usuarios);
                    } else if (inbound.getString(FreeEXProperties.getProperties("msgType")).equals(FreeEXProperties.getProperties("msg"))) {
                        inbound = inbound.getJSONObject(FreeEXProperties.getProperties("users"));
                        String from = inbound.getString(FreeEXProperties.getProperties("from"));
                        String message = inbound.getString(FreeEXProperties.getProperties("msg"));
                        actividad.agregarMensajes(message, from);
                    }
                }catch (IOException e){

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else{
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

package com.example.esteban.freeex;

/**
 * Created by Esteban on 21/11/2015.
 */
import org.json.JSONObject;

public class Parser{

    LQueue bluetoothRequests;
    LQueue clientRequests;
    private static Parser INSTANCE = null;


    private Parser(){
        clientRequests = new LQueue();
        bluetoothRequests = new LQueue();

        Thread bluetoothRequests = (new Thread(ProcessBluetoothRequests.getInstance()));
      //  Thread clientRequests = new Thread(ProcessClientRequests.getInstance());

        bluetoothRequests.start();
      //  clientRequests.start();
    }

    public static Parser getInstance(){
        if(INSTANCE == null){
            INSTANCE = new Parser();
        }
        return INSTANCE;
    }

    // getters and setters

    public synchronized void addBluetoothRequest(JSONObject action) {
        bluetoothRequests.enqueue(action);
    }

    public void addClientRequest(JSONObject action) {
        clientRequests.enqueue(action);
    }
}
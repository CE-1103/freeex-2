package com.example.esteban.freeex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.StaticLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class ListaConversaciones extends AppCompatActivity {

    public static String EXTRA_MESSAGE = "com.example.esteban.freeex.MESSAGE";

    private String[] listaConectados;
    private UsuarioMensaje primerUsuario;
    Intent intent;

    public void setListaConectados(String[] listaConectados1){
        listaConectados = listaConectados1;
        String[] myStringArray=listaConectados;
        ArrayAdapter<String> myAdapter=new
                ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                myStringArray);
        ListView myList=(ListView)
                findViewById(R.id.listView2);
        myList.setAdapter(myAdapter);
    }

    public void agregarMensajes(String mensaje, String from){
        UsuarioMensaje tmp = primerUsuario;
        if (tmp!=null){
            while (tmp!=null){
                if (tmp.getUsuario().equals(from)){
                    tmp.agregarMensaje(mensaje);
                    break;
                }
                tmp = tmp.getNext();
            }
            tmp = new UsuarioMensaje();
            tmp.setUsuario(from);
            tmp.agregarMensaje(mensaje);
            tmp.setNext(this.primerUsuario);
            this.primerUsuario = tmp;
        }
        else{
            this.primerUsuario = new UsuarioMensaje();
            tmp.setUsuario(from);
            tmp.agregarMensaje(mensaje);
        }
    }

    public void difusion(){
        intent = new Intent(this, PantallaMensaje.class);
        intent.putExtra(EXTRA_MESSAGE, "difusion");
        startActivity(intent);
    }

    public void mandarMensaje(String from){
        UsuarioMensaje tmp = primerUsuario;
        if (tmp!=null){
            while (tmp!=null){
                if (tmp.getUsuario().equals(from)){
                    break;
                }
                tmp = tmp.getNext();
            }
            tmp = new UsuarioMensaje();
            tmp.setUsuario(from);
            tmp.setNext(this.primerUsuario);
            this.primerUsuario = tmp;
        }
        else{
            this.primerUsuario = new UsuarioMensaje();
            tmp.setUsuario(from);
            tmp = this.primerUsuario;
        }

        intent = new Intent(this, PantallaMensaje.class);
        intent.putExtra(EXTRA_MESSAGE, from+tmp.getMensajes());
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_conversaciones);

        String[] myStringArray=listaConectados;
        ArrayAdapter<String> myAdapter=new
                ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                myStringArray);
        ListView myList=(ListView)
                findViewById(R.id.listView2);
        myList.setAdapter(myAdapter);

        AdapterView.OnItemClickListener
                mMessageClickedHandler =
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView parent,
                                            View v,
                                            int position,
                                            long id) {
                        mandarMensaje(((TextView) v).getText().toString());
                    }
                };

        myList.setOnItemClickListener(
                mMessageClickedHandler);

        Thread msgReceiver = new Thread(new ThreadInBound(this));
        msgReceiver.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pantalla_conversaciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        JSONObject tmp = new JSONObject();
        try{
            tmp.put(FreeEXProperties.getProperties("msgType"), FreeEXProperties.getProperties("logOut"));
        } catch (IOException e){

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}

package com.example.esteban.freeex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

public class PantallaMensaje extends AppCompatActivity {

    Intent intent;
    String[] mensajes;
    String usuario;
    EditText mensajeAEnviar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_mensaje);
        mensajeAEnviar = (EditText) findViewById(R.id.editText1);
        intent = getIntent();
        String heredado = intent.getStringExtra(ListaConversaciones.EXTRA_MESSAGE);
        if (heredado.equals("difusion")) {
            usuario = heredado;
        }
        else{
            String[] heredadoArray = heredado.split("\\+");
            this.usuario = heredadoArray[0];
            String[] myStringArray = Arrays.copyOfRange(heredadoArray, 1, heredadoArray.length);
            ArrayAdapter<String> myAdapter = new
                    ArrayAdapter<String>(
                    this,
                    android.R.layout.simple_list_item_1,
                    myStringArray);
            ListView myList = (ListView)
                    findViewById(R.id.listView2);
            myList.setAdapter(myAdapter);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pantalla_mensaje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void enviarMensaje(View view){
        String mensaje = mensajeAEnviar.getText().toString();
        JSONObject tmp = new JSONObject();
        if (usuario.equals("difusion")){
            try {
                tmp.put(FreeEXProperties.getProperties("msg"), mensaje);
                tmp.put(FreeEXProperties.getProperties("msgType"), FreeEXProperties.getProperties("dif"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                tmp.put(FreeEXProperties.getProperties("to"), usuario);
                tmp.put(FreeEXProperties.getProperties("msg"), mensaje);
                tmp.put(FreeEXProperties.getProperties("msgType"), FreeEXProperties.getProperties("msg"));
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Parser.getInstance().addClientRequest(tmp);
    }
}

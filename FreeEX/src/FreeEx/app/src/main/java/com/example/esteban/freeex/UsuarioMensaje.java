package com.example.esteban.freeex;

/**
 * Created by Esteban on 22/11/2015.
 */
public class UsuarioMensaje {
    private UsuarioMensaje next;
    private String mensajes;
    private String usuario;

    public UsuarioMensaje getNext() {
        return next;
    }

    public void setNext(UsuarioMensaje next) {
        this.next = next;
    }

    public String getMensajes() {
        return mensajes;
    }

    public void setMensajes(String mensajes) {
        this.mensajes = mensajes;
    }

    public void agregarMensaje(String mensaje){
        if (this.mensajes == null){
            this.mensajes = mensaje;
        }
        else{
            this.mensajes = mensaje + "+" + this.mensajes;
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}

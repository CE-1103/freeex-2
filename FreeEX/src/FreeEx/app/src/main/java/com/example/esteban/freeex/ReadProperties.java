package com.example.esteban.freeex;

/**
 * Created by Esteban on 22/11/2015.
 */
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author Keylor Rojas
 * @version 28/10/2015
 */
public class ReadProperties {

    InputStream input;
    Properties properties;

    public ReadProperties(String file) throws IOException{

        input = getClass().getClassLoader().getResourceAsStream(file);
        if(input != null){
            properties.load(input);
        }
        input.close();
    }

    public String getProperties(String key){
        return properties.getProperty(key);
    }

}
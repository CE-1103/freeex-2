package com.example.esteban.freeex;

/**
 * Created by Esteban on 22/11/2015.
 */
import java.io.IOException;

public class FreeEXProperties {

    private static ReadProperties PROPIEDADES = null;

    private FreeEXProperties(){}

    public static String getProperties(String key) throws IOException{
        if(PROPIEDADES == null){
            PROPIEDADES = new ReadProperties("FreeEX.properties");
        }
        return PROPIEDADES.getProperties(key);
    }

}

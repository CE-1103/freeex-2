package com.example.esteban.freeex;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class PantallaInicio extends AppCompatActivity {


    double longitude;
    double latitude;

    EditText usuario;

    String posicion;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);
        JSONObject tmp = new JSONObject();
        try {
            tmp.put("messageType", "connect");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Parser.getInstance().addClientRequest(tmp);

        String[] myStringArray={"A","B","C"};
        ArrayAdapter<String> myAdapter=new
                ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                myStringArray);
        ListView myList=(ListView)
                findViewById(R.id.listView2);
        myList.setAdapter(myAdapter);

        String[] myStringArray2={"A","B","C","D"};
        ArrayAdapter<String> myAdapter2=new
                ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                myStringArray2);
        myList.setAdapter(myAdapter2);

        LocationManager locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        usuario = (EditText) findViewById(R.id.campoUsuario);

        try{
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
        }
        catch (SecurityException se){
            se.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pantalla_inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    final LocationListener locListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            longitude=location.getLongitude();
            latitude=location.getLatitude();
            posicion = String.valueOf(latitude)+","+String.valueOf(longitude);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };



    public void hacerLogin(View view) throws JSONException {
        String usuarionuevo = usuario.getText().toString();
        JSONObject tmp = new JSONObject();
        try {
            tmp.put(FreeEXProperties.getProperties("loginUser"), usuarionuevo);// manda el usu
            tmp.put(FreeEXProperties.getProperties("msgType"), "login");
            tmp.put(FreeEXProperties.getProperties("location"), posicion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String conectado = (String) (Parser.getInstance().addClientRequest(tmp));
        try {
            if (conectado.equals(FreeEXProperties.getProperties("ok"))){
                intent = new Intent(this, ListaConversaciones.class);
                startActivity(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
